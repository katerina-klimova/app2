package com.example.app.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;
import com.example.app.activities.chat.MessageDataModel;
import com.example.app.activities.chat.OneChatFragment;
import com.example.app.activities.messages.ChatDataModel;
import com.example.app.activities.vetpages.VetPageActivity;
import com.example.app.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter {

private Context context;
private List<ChatDataModel> chatsList;
private List<User> chattedUsers;
private FragmentManager fragmentManager;

    public ChatAdapter(Context context, List<ChatDataModel> chatsList, List<User> chattedUsers, FragmentManager fragmentManager) {
        this.context = context;
        this.chatsList = chatsList;
        this.chattedUsers = chattedUsers;
        this.fragmentManager = fragmentManager;
    }

    public ChatAdapter(Context applicationContext, List<ChatDataModel> chatsList, List<User> userList ) {
        this.context = applicationContext;
        this.chatsList = chatsList;
        this.chattedUsers = userList;
    }


    @Override
    public int getItemCount() {
        return chatsList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_view, parent, false);
        return new ChatViewHolder(v);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.N)
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatViewHolder viewHolder = (ChatViewHolder) holder;
        viewHolder.position = position;

        ChatDataModel chat = chatsList.get(position);
        User vet = chattedUsers.get(position);
        if (chat.getListMessages() != null) {
            ArrayList<MessageDataModel> valuesList = new ArrayList<>(chat.getListMessages().values());
            Collections.sort(valuesList, MessageDataModel.compareByDate.reversed());
            MessageDataModel lastMessage = valuesList.get(0);
            ((ChatViewHolder) holder).lastMessage.setText(lastMessage.getText());
        } else {
            ((ChatViewHolder) holder).lastMessage.setText("");
        }
        ((ChatViewHolder) holder).friendName.setText(vet.getName());
        if (vet.getProfileImage() != null) {
            ImageLoader.setImage(context, ((ChatViewHolder) holder).profileImage,vet.getId(), vet.getProfileImage());
        }
        if ((chat.getCntUnread() != null) && (chat.getCntUnread() > 0)) {
            ((ChatViewHolder) holder).cntUnreadMsgs.setVisibility(View.VISIBLE);
            ((ChatViewHolder) holder).cntUnreadMsgs.setText(String.valueOf(chat.getCntUnread()));
        } else {
            ((ChatViewHolder) holder).cntUnreadMsgs.setVisibility(View.INVISIBLE);
        }
    }

    public final class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView friendName;
        TextView lastMessage;
         TextView cntUnreadMsgs;
        public int position;
        ImageView profileImage;
        ChatDataModel chat;
        User user;

        public ChatViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            chat = chatsList.get(position);
            user = chattedUsers.get(position);
            friendName = itemView.findViewById(R.id.other_user_name);
            lastMessage = itemView.findViewById(R.id.last_message);
            cntUnreadMsgs = itemView.findViewById(R.id.cntUnreadMsges);
            profileImage = itemView.findViewById(R.id.chatImagePreview);

        }

        @Override
        public void onClick(View view) {
            Bundle args = new Bundle();
            args.putSerializable("chat", chat);
            args.putSerializable("user", user);
            if(fragmentManager!=null) {
                Fragment fr = new OneChatFragment();
                FragmentManager fm = fragmentManager;
                androidx.fragment.app.FragmentTransaction ft = fm.beginTransaction();

                fr.setArguments(args);
                ft.replace(R.id.nav_host_fragment, fr);
                ft.commit();
            }

        }
    }
}