package com.example.app.adapters;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

public class ImageLoader {
    public static void uploadImage(Context context, ContentResolver cr, Uri imageUri, DatabaseReference ref){
        StorageReference fileReference = FirebaseUtils.getUserImage()
                .child(ImageLoader.getFileName(imageUri,cr));
        fileReference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                ref.child("profileImage").setValue(ImageLoader.getFileName(imageUri,cr));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Cannot upload file", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public static void setImage(Context context, ImageView target, String pathToImg){
            StorageReference fileReference = FirebaseUtils.getUserImage()
                    .child(pathToImg);
            try {
                File img = File.createTempFile("img", "jpg");
                fileReference.getFile(img).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                       // target.setBackgroundColor(ContextCompat.getColor(context, R.color.colorThird));
                        Picasso.with(context)
                                .load(img).transform(new CircleTransform()).into(target);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public static void setImage(Context context, ImageView target, String userId, String pathToImg){
        StorageReference fileReference = FirebaseUtils.getUserImage(userId)
                .child(pathToImg);
        try {
            File img = File.createTempFile("img", "jpg");
            fileReference.getFile(img).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // target.setBackgroundColor(ContextCompat.getColor(context, R.color.colorThird));
                    Picasso.with(context)
                            .load(img).transform(new CircleTransform()).into(target);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFileName(Uri uri, ContentResolver cr) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = cr.query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

}
