package com.example.app.model;

import com.example.app.activities.messages.ChatDataModel;
import com.example.app.activities.pets.AnimalDataModel;

import java.io.Serializable;
import java.util.Map;

public class UserDataModel extends User implements Serializable {

    private Map<String,AnimalDataModel> animals;

   //private List<ChatDataModel> chats;

    /*public Map<String, ChatDataModel> getChats() {
        return chats;
    }

    public void setChats(Map<String, ChatDataModel> chats) {
        this.chats = chats;
    }

    public List<ChatDataModel> getChats() {
        return chats;
    }

    public void setChats(List<ChatDataModel> chats) {
        this.chats = chats;
    }*/

    /*public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }*/

    public Map<String, AnimalDataModel> getAnimals() {
        return animals;
    }

    public void setAnimals(Map<String, AnimalDataModel> animals) {
        this.animals = animals;
    }

    public UserDataModel(){

    }

}
