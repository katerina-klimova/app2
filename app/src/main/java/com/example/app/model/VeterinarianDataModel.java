package com.example.app.model;

import com.example.app.activities.messages.ChatDataModel;

import java.io.Serializable;
import java.util.Map;

public class VeterinarianDataModel extends User implements Serializable {
    /*private String id;
    private String name;
    private String email;*/
    private String specialization;
    private Integer experience;
   // private String profileImage;
   // private Map<String, ChatDataModel> chats;

    /*public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, ChatDataModel> getChats() {
        return chats;
    }

    public void setChats(Map<String, ChatDataModel> chats) {
        this.chats = chats;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }*/

    public VeterinarianDataModel() {
    }

    /*public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }
}
