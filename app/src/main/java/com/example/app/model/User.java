package com.example.app.model;

import com.example.app.activities.messages.ChatDataModel;

import java.io.Serializable;
import java.util.Map;

public class User implements Serializable {
    protected String id;
    protected String name;
    protected String email;
    protected String profileImage;
    protected Map<String, ChatDataModel> chats;

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Map<String, ChatDataModel> getChats() {
        return chats;
    }

    public void setChats(Map<String, ChatDataModel> chats) {
        this.chats = chats;
    }
}
