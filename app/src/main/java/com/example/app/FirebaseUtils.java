package com.example.app;

import androidx.annotation.NonNull;

import com.example.app.activities.chat.MessageDataModel;
import com.example.app.activities.pets.AnimalDataModel;
import com.example.app.model.VeterinarianDataModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FirebaseUtils {
    private static final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static final FirebaseStorage mStorage = FirebaseStorage.getInstance();
    private String avatarLink;


    public static FirebaseAuth getMAuth() {
        return mAuth;
    }

    public static String getCurrentUserId() {
        return mAuth.getCurrentUser().getUid();
    }

    public static FirebaseDatabase getDatabase() {
        return database;
    }

    public static FirebaseStorage getStorage() {
        return mStorage;
    }

    public static StorageReference getUserImage() {
        return mStorage.getReference(getCurrentUserId());
    }
    public static StorageReference getUserImage(String userId) {
        return mStorage.getReference(userId);
    }

    public static DatabaseReference getUserReference() {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid());
    }

    public static DatabaseReference getPetReference(String id) {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("animals").child(id);
    }



    public static void saveUserToDatabase(String userId, Object value) {
        database.getReference().child("users").child(userId).setValue(value);
    }

    public static void createPet(AnimalDataModel pet) {
        database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("animals").push().setValue(pet);
    }
    public static void updatePet(AnimalDataModel pet) {
        database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("animals").child(pet.getId()).setValue(pet);
    }
    public static DatabaseReference getPetsReference() {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("animals");
    }

    public static DatabaseReference getChatsReference() {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("chats");
    }

    public static DatabaseReference getChatsReference(String userId) {
        return database.getReference().child("users").child(userId)
                .child("chats");
    }

    public static DatabaseReference getChatReference(String chatId) {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("chats").child(chatId);
    }

    public static Query getMessages(String chatId) {
        return database.getReference().child("users").child(getMAuth().getCurrentUser().getUid())
                .child("chats").child(chatId).child("listMessages").orderByChild("dateTime").limitToLast(50);
    }

    public static void saveMessageToDatabase(MessageDataModel message, String vetId) {
        getChatReference(vetId).child("listMessages").push().setValue(message);
        searchVeterinariansById(vetId).child("chats").child(getCurrentUserId()).child("listMessages").push().setValue(message);
    }

    public static void resetUnread(String chatId) {
        getChatReference(chatId).child("cntUnread").setValue(0);
    }

    public static void readMessages(String friendChatId) {

    }

    public static Query searchVeterinarians(String specialization) {
        return database.getReference().child("veterinarians").orderByChild("specialization").startAt(specialization);
    }

    public static DatabaseReference searchVeterinarians() {
        return database.getReference().child("veterinarians");
    }

    public static DatabaseReference searchVeterinariansById(String id) {
        return database.getReference().child("veterinarians").child(id);
    }

    public static void saveVeterinarianToDatabase(String uid, VeterinarianDataModel value) {
        database.getReference().child("veterinarians").child(uid).setValue(value);
    }

    public String getAvatarLink() {
        database.getReference().child("users").child(getCurrentUserId()).child("avatarLink").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                avatarLink = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return avatarLink;
    }

    public static void setAvatarLink(String link) {
        database.getReference().child("users").child(getCurrentUserId()).child("avatarLink").setValue(link);
    }
}
