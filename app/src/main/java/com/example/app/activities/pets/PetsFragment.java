package com.example.app.activities.pets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.adapters.ImageLoader;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class PetsFragment extends Fragment {

    private PetsViewModel petsViewModel;
    private PetViewAdapter adapter;
    private List<AnimalDataModel> animals = new ArrayList<>();
    private FirebaseDatabase database = FirebaseUtils.getDatabase();
    private FirebaseUser user = FirebaseUtils.getMAuth().getCurrentUser();
    public PetsFragment() {
        // Required empty public constructor
    }
    private View.OnClickListener addPetListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, new PetCardFragment() ); // give your fragment container id in first parameter
            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
            transaction.commit();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        FirebaseUtils.getPetsReference().addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        animals.clear();
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            AnimalDataModel animal = data.getValue(AnimalDataModel.class);
                            animal.setId(data.getKey());
                            animals.add(animal);
                        }

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        petsViewModel =
                ViewModelProviders.of(this).get(PetsViewModel.class);

        View root = inflater.inflate(R.layout.fragment_pets, container, false);
        // находим список
        RecyclerView listPetsView =root.findViewById(R.id.listPetsView);
        FloatingActionButton addPetButton = root.findViewById(R.id.addPetButton);
        addPetButton.setOnClickListener(addPetListener);
        // создаем адаптер
         adapter = new PetViewAdapter();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        listPetsView.setLayoutManager(llm);
        adapter.notifyDataSetChanged();
        listPetsView.setAdapter(adapter);
        return root;
    }

    private class PetViewAdapter extends RecyclerView.Adapter {


        @Override
        public int getItemCount() {
            return animals.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pet_view, parent, false);
            PetViewHolder pvh = new PetViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            PetViewHolder viewHolder = (PetViewHolder) holder;
            viewHolder.position = position;
            AnimalDataModel pet = animals.get(position);
            ((PetViewHolder) holder).setPet(pet);
            ((PetViewHolder) holder).title.setText(pet.getName());
            if(pet.getProfileImage()!=null){
                ImageLoader.setImage(getContext(),((PetViewHolder) holder).petImage,pet.getProfileImage());
            }
        }

        public final  class PetViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView title;
            public int position;
            ImageView petImage;
            private AnimalDataModel pet;

            public AnimalDataModel getPet() {
                return pet;
            }

            public void setPet(AnimalDataModel pet) {
                this.pet = pet;
            }

            public PetViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                title = (TextView) itemView.findViewById(R.id.pet_name);
                petImage = (ImageView) itemView.findViewById(R.id.petImagePreview);
            }

            @Override
            public void onClick(View view) {
                Fragment fr = new PetCardFragment();
                FragmentManager fm = getFragmentManager();
                androidx.fragment.app.FragmentTransaction ft = fm.beginTransaction();
                Bundle args = new Bundle();
                args.putSerializable("pet", pet);
                fr.setArguments(args);
                ft.replace(R.id.nav_host_fragment, fr);
                ft.commit();
            }
        }
    }
}
