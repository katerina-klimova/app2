package com.example.app.activities.vetpages;

import android.os.Bundle;

import com.example.app.activities.messages.ChatDataModel;
import com.example.app.adapters.ChatAdapter;
import com.example.app.model.User;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;

import java.util.ArrayList;
import java.util.List;

public class VetPageActivity extends AppCompatActivity {
    public List<ChatDataModel> chatsList = new ArrayList<>();
    public List<User> userList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vet_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView listDialogs = findViewById(R.id.listVetChats);
        ChatAdapter adapter = new ChatAdapter(getApplicationContext(),chatsList, userList);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listDialogs.setLayoutManager(llm);
        adapter.notifyDataSetChanged();
        listDialogs.setAdapter(adapter);
    }
}
