package com.example.app.activities.messages;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.chat.MessageDataModel;
import com.example.app.activities.chat.OneChatFragment;
import com.example.app.adapters.ChatAdapter;
import com.example.app.model.User;
import com.example.app.model.VeterinarianDataModel;
import com.example.app.adapters.ImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessagesFragment extends Fragment {
    private ChatAdapter adapter;
    public List<ChatDataModel> chatsList = new ArrayList<>();
    public List<User> vetsList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_messages, container, false);
        RecyclerView listChatsView = root.findViewById(R.id.listChatsView);
        adapter = new ChatAdapter(getContext(),chatsList,vetsList,getFragmentManager());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        listChatsView.setLayoutManager(llm);
        adapter.notifyDataSetChanged();

        listChatsView.setAdapter(adapter);
        //setListeners();
        return root;
    }

    private void setListeners(){
        FirebaseUtils.getDatabase().getReference().addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        chatsList.clear();
                        vetsList.clear();
                        for (DataSnapshot data : dataSnapshot.child("users").child(FirebaseUtils.getCurrentUserId()).child("chats").getChildren()) {
                            ChatDataModel chat = data.getValue(ChatDataModel.class);
                            chat.setId(data.getKey());
                            DataSnapshot dataVet = (DataSnapshot) dataSnapshot.child("veterinarians").child(chat.getId());
                            VeterinarianDataModel veterinarianDataModel = dataVet.getValue(VeterinarianDataModel.class);
                            veterinarianDataModel.setId(dataVet.getKey());
                            vetsList.add(veterinarianDataModel);
                            chatsList.add(chat);
                        }

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });


    }

    @Override
    public void onResume() {
        super.onResume();
        setListeners();
    }

    /*public class ChatFragmentAdapter extends RecyclerView.Adapter {


        @Override
        public int getItemCount() {
            return chatsList.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_view, parent, false);
            return new ChatViewHolder(v);
        }

        @Override
        @TargetApi(Build.VERSION_CODES.N)
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ChatViewHolder viewHolder = (ChatViewHolder) holder;
            viewHolder.position = position;

            ChatDataModel chat = chatsList.get(position);
            VeterinarianDataModel vet = vetsList.get(position);
            if (chat.getListMessages() != null) {
                ArrayList<MessageDataModel> valuesList = new ArrayList<>(chat.getListMessages().values());
                Collections.sort(valuesList, MessageDataModel.compareByDate.reversed());
                MessageDataModel lastMessage = valuesList.get(0);
                ((ChatViewHolder) holder).lastMessage.setText(lastMessage.getText());
            } else {
                ((ChatViewHolder) holder).lastMessage.setText("");
            }
            ((ChatViewHolder) holder).friendName.setText(vet.getName());
            if (vet.getProfileImage() != null) {
                ImageLoader.setImage(getContext(), ((ChatViewHolder) holder).profileImage,vet.getId(), vet.getProfileImage());
            }
            if ((chat.getCntUnread() != null) && (chat.getCntUnread() > 0)) {
                ((ChatViewHolder) holder).cntUnreadMsgs.setVisibility(View.VISIBLE);
                ((ChatViewHolder) holder).cntUnreadMsgs.setText(String.valueOf(chat.getCntUnread()));
            } else {
                ((ChatViewHolder) holder).cntUnreadMsgs.setVisibility(View.INVISIBLE);
            }
        }

        public final class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView friendName;
            TextView lastMessage;
            TextView cntUnreadMsgs;
            public int position;
            ImageView profileImage;
             ChatDataModel chat;
            VeterinarianDataModel vet;

            public ChatViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                chat = chatsList.get(position);
                vet = vetsList.get(position);
                friendName = itemView.findViewById(R.id.other_user_name);
                lastMessage = itemView.findViewById(R.id.last_message);
                cntUnreadMsgs = itemView.findViewById(R.id.cntUnreadMsges);
                profileImage = itemView.findViewById(R.id.chatImagePreview);

            }

            @Override
            public void onClick(View view) {
                Fragment fr = new OneChatFragment();
                FragmentManager fm = getFragmentManager();
                androidx.fragment.app.FragmentTransaction ft = fm.beginTransaction();
                Bundle args = new Bundle();
                args.putSerializable("chat", chat);
                args.putSerializable("veterinarian", vet);
                fr.setArguments(args);
                ft.replace(R.id.nav_host_fragment, fr);
                ft.commit();
            }
        }
    }*/

}
