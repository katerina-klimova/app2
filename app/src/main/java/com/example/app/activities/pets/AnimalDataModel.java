package com.example.app.activities.pets;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;

public class AnimalDataModel implements Serializable {
    private String id;
    private String name;
    private String kind;
    private String breed;
    private Double age;
    private String sex;
    private Date birthday;
    private String wool;
    private Double weight;
    private String sterilization;
    private String chipNumber;
    private Date degelmintization;
    private Date fleaVaccination;
    private Date commonVaccination;
    private Date rabieVaccination;
    private String info;
    private String profileImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public AnimalDataModel(@Nullable String name,@Nullable String kind,
                           @Nullable String breed,@Nullable Double age) {
        this.name = name;
        this.kind = kind;
        this.breed = breed;
        this.age = age;
    }

    public AnimalDataModel(){

    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getWool() {
        return wool;
    }

    public void setWool(String wool) {
        this.wool = wool;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getSterilization() {
        return sterilization;
    }

    public void setSterilization(String sterilization) {
        this.sterilization = sterilization;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public Date getDegelmintization() {
        return degelmintization;
    }

    public void setDegelmintization(Date degelmintization) {
        this.degelmintization = degelmintization;
    }

    public Date getFleaVaccination() {
        return fleaVaccination;
    }

    public void setFleaVaccination(Date fleaVaccination) {
        this.fleaVaccination = fleaVaccination;
    }

    public Date getCommonVaccination() {
        return commonVaccination;
    }

    public void setCommonVaccination(Date commonVaccination) {
        this.commonVaccination = commonVaccination;
    }

    public Date getRabieVaccination() {
        return rabieVaccination;
    }

    public void setRabieVaccination(Date rabieVaccination) {
        this.rabieVaccination = rabieVaccination;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
