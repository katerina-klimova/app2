package com.example.app.activities.pets;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.NavigationUtils;
import com.example.app.adapters.CircleTransform;
import com.example.app.adapters.ImageLoader;
import com.google.android.gms.common.util.Strings;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PetCardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PetCardFragment extends Fragment {
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int RESULT_OK           = -1;
    private EditText name;
    private Spinner kind;
    private EditText breed;
    private EditText age;
    private Spinner sex;
    private EditText birthday;
    private Spinner wool;
    private EditText weight;
    private Spinner sterilization;
    private EditText chipNumber;
    private EditText degelment;
    private EditText fleaVac;
    private EditText commonVac;
    private EditText rabieVac;
    private EditText info;
    private ImageView petAvatar;
    private Uri petAvatarUri;

    private AnimalDataModel pet;
    private AnimalDataModel updatedPet;
    private View root;

    FirebaseDatabase database = FirebaseUtils.getDatabase();
    String currentUserId = FirebaseUtils.getMAuth().getCurrentUser().getUid();

    public PetCardFragment() {
        // Required empty public constructor
    }

    public static PetCardFragment newInstance(String param1, String param2) {
        PetCardFragment fragment = new PetCardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public View.OnClickListener addPetListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            updatedPet.setName(name.getText().toString());
            updatedPet.setKind(kind.getSelectedItem().toString());
            updatedPet.setBreed(breed.getText().toString());
            updatedPet.setAge(stringToDouble(age.getText().toString()));
            updatedPet.setSex(sex.getSelectedItem().toString());
            updatedPet.setBirthday(stringToDate(birthday.getText().toString()));
            updatedPet.setWool(wool.getSelectedItem().toString());
            updatedPet.setWeight(stringToDouble(weight.getText().toString()));
            updatedPet.setSterilization(sterilization.getSelectedItem().toString());
            updatedPet.setChipNumber(chipNumber.getText().toString());
            updatedPet.setDegelmintization(stringToDate(degelment.getText().toString()));
            updatedPet.setFleaVaccination(stringToDate(fleaVac.getText().toString()));
            updatedPet.setCommonVaccination(stringToDate(commonVac.getText().toString()));
            updatedPet.setRabieVaccination(stringToDate(fleaVac.getText().toString()));
            updatedPet.setInfo(info.getText().toString());
            if (pet == null) {
                FirebaseUtils.createPet(updatedPet);
            } else {
                FirebaseUtils.updatePet(updatedPet);
            }
            if(updatedPet.getProfileImage()!=null && pet.getProfileImage()==null){
                ImageLoader.uploadImage(getContext(),getContext().getContentResolver(),petAvatarUri,
                        FirebaseUtils.getPetReference(pet.getId()));
            }
            NavigationUtils.openNextFragment(getFragmentManager(), R.id.nav_host_fragment, new PetsFragment());

        }
    };

    public View.OnClickListener cancelListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            NavigationUtils.openNextFragment(getFragmentManager(), R.id.nav_host_fragment, new PetsFragment());
        }
    };

    private Date stringToDate(String date) {
        Date res = null;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            res = formatter.parse(date);
        } catch (ParseException e) {
            Toast.makeText(getContext(), "Cannot parse some data!", Toast.LENGTH_SHORT);
        }
        return res;
    }

    private Double stringToDouble(String number){
        if(!Strings.isEmptyOrWhitespace(number)){
            return Double.valueOf(number);
        }
         return null;
    }

    private String doubleToString(Double number){
        if(number!=null){
            return number.toString();
        }
        return null;
    }

    private String dateToString(Date date){
        if(date!=null){
            return date.toString();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_edit_pet, container, false);
        Button confirmAddPet = root.findViewById(R.id.confirmAddPet);
        AppCompatImageButton cancelAddPet = root.findViewById(R.id.cancelAddPet);
        updatedPet = new AnimalDataModel();
        initializeFields();
        setPetOrCreate();

        cancelAddPet.setOnClickListener(cancelListener);
        confirmAddPet.setOnClickListener(addPetListener);
        return root;
    }

    private void initializeFields() {
        name = root.findViewById(R.id.inpPetName);
        kind = root.findViewById(R.id.inpPetKind);
        kind.setAdapter(getAdapter(root.getContext(), AnimalCardLists.animalKinds));
        breed = root.findViewById(R.id.inpPetBreed);
        age = root.findViewById(R.id.inpPetAge);
        sex = root.findViewById(R.id.inpSex);
        sex.setAdapter(getAdapter(root.getContext(), AnimalCardLists.sex));
        birthday = root.findViewById(R.id.inpBirthdate);
        wool = root.findViewById(R.id.inpWool);
        wool.setAdapter(getAdapter(root.getContext(), AnimalCardLists.wool));
        weight = root.findViewById(R.id.inpWeight);
        sterilization = root.findViewById(R.id.inpSteril);
        sterilization.setAdapter(getAdapter(root.getContext(), AnimalCardLists.sterilization));
        chipNumber = root.findViewById(R.id.inpChipNum);
        fleaVac = root.findViewById(R.id.inpFlea);
        degelment = root.findViewById(R.id.inpDegelmintDate);
        commonVac = root.findViewById(R.id.inpVaccination);
        rabieVac = root.findViewById(R.id.inpRabie);
        info = root.findViewById(R.id.inpInfo);
        petAvatar =root.findViewById(R.id.petProfileImage);
        petAvatar.setOnClickListener(upldAvatar);
    }

    private void setPetOrCreate() {
        Bundle args = getArguments();
        updatedPet = new AnimalDataModel();
        if (args != null && args.containsKey("pet")) {
            pet = (AnimalDataModel) args.getSerializable("pet");
            name.setText(pet.getName());
            kind.setSelection(AnimalCardLists.animalKinds.indexOf(pet.getKind()));
            sex.setSelection(AnimalCardLists.sex.indexOf(pet.getSex()));
            age.setText(doubleToString(pet.getAge()));
            breed.setText(pet.getBreed());
            birthday.setText(dateToString(pet.getBirthday()));
            wool.setSelection(AnimalCardLists.wool.indexOf(pet.getWool()));
            weight.setText(doubleToString(pet.getWeight()));
            sterilization.setSelection(AnimalCardLists.sterilization.indexOf(pet.getSterilization()));
            chipNumber.setText(pet.getChipNumber());
            degelment.setText(dateToString(pet.getDegelmintization()));
            fleaVac.setText(dateToString(pet.getFleaVaccination()));
            commonVac.setText(dateToString(pet.getCommonVaccination()));
            rabieVac.setText(dateToString(pet.getRabieVaccination()));
            updatedPet.setId(pet.getId());
            if(pet.getProfileImage()!=null){
                updatedPet.setProfileImage(pet.getProfileImage());
                ImageLoader.setImage(getContext(),petAvatar,pet.getProfileImage());
            }
        }
    }
    public View.OnClickListener upldAvatar = v -> openChooser();
    private ArrayAdapter<?> getAdapter(Context context, String[] list) {
        ArrayAdapter<?> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private ArrayAdapter<?> getAdapter(Context context, List<String> list) {
        ArrayAdapter<?> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private void openChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == PICK_IMAGE_REQUEST) && (resultCode == RESULT_OK) && (data != null)
                && (data.getData() != null)) {
            petAvatarUri = data.getData();
            petAvatar.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorThird));
            Picasso.with(getContext())
                    .load(petAvatarUri).transform(new CircleTransform()).into(petAvatar);
            /*ImageLoader.uploadImage(getContext(),getContext().getContentResolver(),data.getData(),
                    FirebaseUtils.getPetReference(pet.getId()));*/

        }}

}
