package com.example.app.activities.chat;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.model.User;
import com.example.app.adapters.ImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class OneChatFragment extends Fragment {
    private static int MS_TYPE_RIGHT = 1;
    private static int MS_TYPE_LEFT = 0;
private RecyclerView listMessagesView;
private MessageAdapter adapter;
    private int cntUnread = 0;
    private static  Boolean UNREAD = false;
    User otherUser;

    public OneChatFragment() {
        // Required empty public constructor
    }

    private AppCompatImageButton sendMsg;
    private EditText message;
    private List<MessageDataModel> messages ;
    private Query refToChat;
    private TextView name;
    private ImageView profileImage;


    public static OneChatFragment newInstance(String param1, String param2) {
        OneChatFragment fragment = new OneChatFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_one_chat, container, false);
        sendMsg = root.findViewById(R.id.btnSendMsg);
        message = root.findViewById(R.id.newMessage);
        messages=new ArrayList<>();
        profileImage = root.findViewById(R.id.userImageInChat);
        name = root.findViewById(R.id.userChatName);
        listMessagesView=root.findViewById(R.id.listChatMessages);
        Bundle args = getArguments();
         otherUser = (User) args.getSerializable("user");
        if (otherUser.getProfileImage() != null) {
            ImageLoader.setImage(getContext(), profileImage, otherUser.getId(), otherUser.getProfileImage());
        }
        name.setText(otherUser.getName());
        refToChat = FirebaseUtils.getMessages(otherUser.getId());
        adapter = new MessageAdapter();

        sendMsg.setOnClickListener(sendMessageListener);
        refToChat.addValueEventListener(newMessagesListener);

        listMessagesView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setStackFromEnd(true);
        listMessagesView.setLayoutManager(llm);
        for(MessageDataModel msg:messages){
            msg.setRead(true);
        }
        adapter.notifyDataSetChanged();
        listMessagesView.setAdapter(adapter);
        return root;
    }

    private View.OnClickListener sendMessageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            MessageDataModel msg = new MessageDataModel(FirebaseUtils.getCurrentUserId(),
                    dateFormat.format(new Date()), message.getText().toString());
            FirebaseUtils.saveMessageToDatabase(msg, otherUser.getId());
            if( cntUnread>0)  {cntUnread +=1;}else{ cntUnread=1;}
            FirebaseUtils.getChatReference(otherUser.getId()).child("cntUnread").setValue(cntUnread);
            message.setText("");
        }
    };

    ValueEventListener newMessagesListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            messages.clear();
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                MessageDataModel messg = data.getValue(MessageDataModel.class);
                messg.setId(data.getKey());
                messages.add(messg);
            }
            adapter.notifyDataSetChanged();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private class MessageAdapter extends RecyclerView.Adapter {
        @Override
        public int getItemCount() {
            return messages.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if(viewType == MS_TYPE_LEFT) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_friend_message, parent, false);
                return new MessageViewHolder(v);
            }
            else {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_message, parent, false);
                return new MessageViewHolder(v);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MessageViewHolder viewHolder = (MessageViewHolder) holder;
            viewHolder.position = position;
            MessageDataModel message = messages.get(position);
            ((MessageViewHolder) holder).msgTextView.setText(message.getText());
        }

        public final class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView msgTextView;
            public int position;

            public MessageViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                msgTextView = itemView.findViewById(R.id.showMsg);
            }

            @Override
            public void onClick(View view) {

            }
        }

        @Override
        public int getItemViewType(int position) {
            if (messages.get(position).getSender().equals(FirebaseUtils.getCurrentUserId())) {
                return MS_TYPE_RIGHT;
            } else {
                return MS_TYPE_LEFT;
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseUtils.resetUnread(otherUser.getId());
    }
}
