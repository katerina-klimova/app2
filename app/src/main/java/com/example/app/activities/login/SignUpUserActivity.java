package com.example.app.activities.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.UserPageActivity;
import com.example.app.model.UserDataModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SignUpUserActivity extends AppCompatActivity {
    final FirebaseDatabase database = FirebaseUtils.getDatabase();
    final FirebaseAuth mAuth = FirebaseUtils.getMAuth();
    public static final int GET_FROM_GALLERY = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_user);
        final EditText username = findViewById(R.id.usernameTxt);
        final EditText email = findViewById(R.id.emailTxt);
        final EditText password = findViewById(R.id.passwordTxt);
        final EditText passwordConfirm = findViewById(R.id.confirmPassTxt);
        final Button registerBtn = findViewById(R.id.registerBtn);
        final ProgressBar loadingProgressBar = findViewById(R.id.progressBarImg);

        final boolean[] correctName = {false};
        final boolean[] correctPass = {false};

        password.setHint("Password must be 6 symbols at least!");
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                    correctName[0] = !username.getText().toString().isEmpty();
                correctPass[0] = password.getText().toString().equals(passwordConfirm.getText().toString());
            }
        };
        username.addTextChangedListener(afterTextChangedListener);
        passwordConfirm.addTextChangedListener(afterTextChangedListener);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (correctName[0] && correctPass[0]) {
                    loadingProgressBar.setVisibility(View.VISIBLE);

                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    loadingProgressBar.setVisibility(View.INVISIBLE);
                                    if (task.isSuccessful()) {
                                        FirebaseUser authUser = mAuth.getCurrentUser();
                                        UserDataModel user = new UserDataModel();
                                        user.setEmail( email.getText().toString());
                                        user.setName(username.getText().toString());
                                        FirebaseUtils.saveUserToDatabase(authUser.getUid(),user);
                                       updateUI(UserPageActivity.class);
                                    } else {
                                        Toast.makeText(getApplicationContext(),
                                                "Register Failed...", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                } else {
                    if(!correctName[0]){
                        username.setHighlightColor(Color.RED);
                        username.setFocusable(true);
                    }

                    if(!correctPass[0]){
                        passwordConfirm.setHighlightColor(Color.RED);
                        passwordConfirm.setFocusable(true);
                    }
                }
            }
        });

    }

    public void updateUI(Class<?> activityTo) {
        Intent intent = new Intent(SignUpUserActivity.this, activityTo);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (FileNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "File not found...", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(),
                        "Cannot upload file...", Toast.LENGTH_LONG).show();
            }
        }
    }




}
