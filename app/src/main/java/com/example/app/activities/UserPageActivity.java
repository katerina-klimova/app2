package com.example.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.login.LoginActivity;
import com.example.app.model.UserDataModel;
import com.example.app.adapters.ImageLoader;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class UserPageActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private FirebaseAuth auth = FirebaseUtils.getMAuth();
    private NavigationView navigationView;
    private TextView headUserName;
    private ImageView avatarView;
    private static final int PICK_IMAGE_REQUEST = 1;
    UserDataModel user;
    DatabaseReference refToUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (auth.getCurrentUser() != null) {
            setContentView(R.layout.activity_user_page);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            navigationView = findViewById(R.id.nav_view);

            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_header_user_page,
                    R.id.nav_pets, R.id.nav_messages, R.id.nav_search, R.id.nav_clinics,
                    R.id.one_chat,R.id.add_pet_fragment,R.id.action_log_out)
                    .setDrawerLayout(drawer)
                    .build();
            navigationView = findViewById(R.id.nav_view);
            setHeader();
            navigationView.getMenu().findItem(R.id.action_log_out).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    logout();
                    return true;
                }
            });
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);
        } else {
            updateUI(LoginActivity.class);
        }
    }

    private void setHeader() {
        refToUser = FirebaseUtils.getUserReference();
        refToUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(UserDataModel.class);
                View headerView = navigationView.getHeaderView(0);
                headUserName = headerView.findViewById(R.id.headUserName);
                headUserName.setText(user.getName());
                avatarView = headerView.findViewById(R.id.imageUserAvatar);
                avatarView.setOnClickListener(upldAvatar);
                showUserAvatar();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_page, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void updateUI(Class<?> activityTo) {
        Intent intent = new Intent(UserPageActivity.this, activityTo);
        startActivity(intent);
    }

    private void logout() {
        auth.signOut();
        Intent intent = new Intent(UserPageActivity.this, LoginActivity.class);
        startActivity(intent);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    public View.OnClickListener upldAvatar = v -> openChooser();

   private void openChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == PICK_IMAGE_REQUEST) && (resultCode == RESULT_OK) && (data != null)
                && (data.getData() != null)) {
            ImageLoader.uploadImage(getApplicationContext(),getContentResolver(),data.getData(),refToUser);
            ImageLoader.setImage(getApplicationContext(),avatarView,user.getProfileImage());


        }
    }

    private void showUserAvatar() {
        if (user.getProfileImage() != null) {
            ImageLoader.setImage(getApplicationContext(),avatarView,user.getProfileImage());
        }
    }
}
