package com.example.app.activities.pets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.HashMap;
import java.util.Map;

public class PetsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    private String[] names = { };

    private Map<String,AnimalDataModel> animalDataModels;

    public PetsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is pets fragment");
        animalDataModels = new HashMap<String,AnimalDataModel>();
    }

    public LiveData<String> getText() {
        return mText;
    }

    public String[] getNames() {
        return names;
    }

    public MutableLiveData<String> getmText() {
        return mText;
    }

    public void setmText(MutableLiveData<String> mText) {
        this.mText = mText;
    }

    public void setNames(String[] names) {
        this.names = names;
    }

    public Map<String,AnimalDataModel> getAnimalDataModels() {
        return animalDataModels;
    }

    public void setAnimalDataModels(Map<String,AnimalDataModel> animalDataModels) {
        this.animalDataModels = animalDataModels;
    }

    public void insertAnimalDataModel(AnimalDataModel animalDataModel) {
        this.animalDataModels.put(animalDataModel.getId(),animalDataModel);
    }

    public void updateAnimalDataModel(AnimalDataModel animalDataModel) {
        this.animalDataModels.remove(animalDataModel.getId());
        insertAnimalDataModel(animalDataModel);
    }
}