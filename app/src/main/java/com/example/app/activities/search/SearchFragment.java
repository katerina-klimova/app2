package com.example.app.activities.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.chat.OneChatFragment;
import com.example.app.adapters.ImageLoader;
import com.example.app.model.VeterinarianDataModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {
    View root;
    private SearchViewModel searchViewModel;
    Query refToVets;
    List<VeterinarianDataModel> veterinars = new ArrayList<>();
    SearchResultAdapter adapter;
    private RecyclerView searchList;
    private CheckBox cbTer;
    private CheckBox cbDiet;
    private CheckBox cbPsyh;
    private CheckBox cbDerm;
    private CheckBox cbRat;
    private List<CheckBox> checks;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        searchViewModel =
                ViewModelProviders.of(this).get(SearchViewModel.class);
        root = inflater.inflate(R.layout.fragment_search_doctor, container, false);
        adapter = new SearchResultAdapter();
        searchList = root.findViewById(R.id.searchListView);
        //FirebaseUtils.searchVeterinarians().addListenerForSingleValueEvent(usersListener);
        setCheckBoxesBehavior();
        search();
        return root;
    }

    private void setCheckBoxesBehavior() {
        checks = new ArrayList<>();
        cbTer = root.findViewById(R.id.cbTerapevt);
        cbDerm = root.findViewById(R.id.cbDerm);
        cbDiet = root.findViewById(R.id.cbDiet);
        cbPsyh = root.findViewById(R.id.cbPsyholog);
        cbRat = root.findViewById(R.id.cbRat);
        checks.add(cbDerm);
        checks.add(cbDiet);
        checks.add(cbPsyh);
        checks.add(cbRat);
        checks.add(cbTer);

        for(CheckBox cb:checks){
            cb.setOnCheckedChangeListener(checkChangeListener);
        }
    }
    CompoundButton.OnCheckedChangeListener checkChangeListener = new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        veterinars.clear();
        if(cbTer.isChecked()){
            FirebaseUtils.searchVeterinarians("терапевт").addListenerForSingleValueEvent(usersListener);
        }
        if(cbDerm.isChecked()){
            FirebaseUtils.searchVeterinarians("дерматолог").addListenerForSingleValueEvent(usersListener);
        }
        if(cbDiet.isChecked()){
            FirebaseUtils.searchVeterinarians("диетолог").addValueEventListener(usersListener);
        }
        if(cbPsyh.isChecked()){
            FirebaseUtils.searchVeterinarians("зоопсихолог").addValueEventListener(usersListener);
        }
        if(cbRat.isChecked()){
            FirebaseUtils.searchVeterinarians("ратолог").addValueEventListener(usersListener);
        }
        search();
    }

    };

    ValueEventListener usersListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot data : dataSnapshot.getChildren()) {
                VeterinarianDataModel user = data.getValue(VeterinarianDataModel.class);
                user.setId(data.getKey());
                veterinars.add(user);
            }
            //adapter.notifyDataSetChanged();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    public class SearchResultAdapter extends RecyclerView.Adapter {
        @NonNull
        @Override

        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_card_header, parent, false);
            return new CardViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            CardViewHolder viewHolder = (CardViewHolder) holder;
            viewHolder.position = position;
            VeterinarianDataModel vet = veterinars.get(position);
            ((CardViewHolder) holder).name.setText(vet.getName());
            ((CardViewHolder) holder).experience.setText("Опыт: " + vet.getExperience().toString()+" лет");
            ((CardViewHolder) holder).specialization.setText(vet.getSpecialization());
            if(vet.getProfileImage()!=null){
                ImageLoader.setImage(getContext(),((CardViewHolder) holder).vetImage,vet.getId(),vet.getProfileImage());
            }
        }

        @Override
        public int getItemCount() {
            return veterinars.size();
        }

        public final class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView name;
            TextView experience;
            TextView specialization;
            Button goChat;
            public int position;
            ImageView vetImage;
            VeterinarianDataModel vet;

            public CardViewHolder(View itemView) {
                super(itemView);
                vet = veterinars.get(position);
                itemView.setOnClickListener(this);
                name = itemView.findViewById(R.id.viewCardName);
                experience = itemView.findViewById(R.id.viewCardExp);
                vetImage = itemView.findViewById(R.id.vetImagePreview);
                goChat = itemView.findViewById(R.id.btGoChat);
                specialization=itemView.findViewById(R.id.viewCardSpecialization);

            }

            @Override
            public void onClick(View view) {
                Fragment fr = new OneChatFragment();
                FragmentManager fm = getFragmentManager();
                androidx.fragment.app.FragmentTransaction ft = fm.beginTransaction();
                Bundle args = new Bundle();
                args.putSerializable("veterinarian", vet);
                fr.setArguments(args);
                ft.replace(R.id.nav_host_fragment, fr);
                ft.commit();
            }
        }
    }

    public void search() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setStackFromEnd(true);
        searchList.setLayoutManager(llm);
        adapter.notifyDataSetChanged();
        searchList.setAdapter(adapter);
    }
}
