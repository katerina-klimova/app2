package com.example.app.activities.messages;

import com.example.app.activities.chat.MessageDataModel;

import java.io.Serializable;
import java.util.Map;

public class ChatDataModel implements Serializable {
    private String id;
    private String userId;
    private Map<String ,MessageDataModel> listMessages;
    private Long cntUnread;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCntUnread() {
        return cntUnread;
    }

    public void setCntUnread(Long cntUnread) {
        this.cntUnread = cntUnread;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, MessageDataModel> getListMessages() {
        return listMessages;
    }

    public void setListMessages(Map<String, MessageDataModel> listMessages) {
        this.listMessages = listMessages;
    }

    public ChatDataModel() {
    }
}
