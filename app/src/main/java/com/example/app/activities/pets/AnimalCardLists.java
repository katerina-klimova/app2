package com.example.app.activities.pets;

import java.util.ArrayList;
import java.util.List;

public class AnimalCardLists {
    public static final List<String> sterilization = new ArrayList<String>(){{add("");add("Да");add("Нет");}};
    public static final List<String> animalKinds = new ArrayList<String>(){{add("");add("Кошка");
    add("Собака");add("Грызун");add("Рептилия");add("Другое");}};
    public static final List<String> sex = new ArrayList<String>(){{add("");add("Мальчик");add("Девочка");}};
    public static final List<String> wool = new ArrayList<String>(){{add("");add("Короткая");
    add("Средняя");add("Длинная");add("Нет");}};

}
