package com.example.app.activities.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.example.app.R;
import com.example.app.activities.UserPageActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ChooseRegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_register);
        RadioButton rbUser = findViewById(R.id.rbUser);
        RadioButton rbVet = findViewById(R.id.rbVeterinar);
        FloatingActionButton back = findViewById(R.id.actionBack);
        FloatingActionButton goRegister = findViewById(R.id.actionGoRegister);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(LoginActivity.class);
            }
        });
        goRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rbUser.isChecked()){
                    updateUI(SignUpUserActivity.class);
                }
                if(rbVet.isChecked()){
                    updateUI(SignUpVeterinarianActivity.class);
                }
            }
        });
    }
    public void updateUI(Class<?> activityTo) {
        Intent intent = new Intent(ChooseRegisterActivity.this, activityTo);
        startActivity(intent);
    }
}
