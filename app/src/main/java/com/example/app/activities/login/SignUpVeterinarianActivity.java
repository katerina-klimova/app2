package com.example.app.activities.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.app.FirebaseUtils;
import com.example.app.R;
import com.example.app.activities.vetpages.VetPageActivity;
import com.example.app.model.VeterinarianDataModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignUpVeterinarianActivity extends AppCompatActivity {
    final FirebaseAuth mAuth = FirebaseUtils.getMAuth();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_veterinarian);
        final EditText username = findViewById(R.id.usernameTxt);
        final EditText email = findViewById(R.id.emailTxt);
        final EditText password = findViewById(R.id.passwordTxt);
        final EditText passwordConfirm = findViewById(R.id.confirmPassTxt);
        final EditText experience = findViewById(R.id.vetExperience);
        final EditText specialization = findViewById(R.id.inpSpecialization);
        final Button registerBtn = findViewById(R.id.registerBtn);
        final ProgressBar loadingProgressBar = findViewById(R.id.progressBarImg);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);

                mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                loadingProgressBar.setVisibility(View.INVISIBLE);
                                if (task.isSuccessful()) {
                                    FirebaseUser authUser = mAuth.getCurrentUser();
                                    VeterinarianDataModel user = new VeterinarianDataModel();
                                    user.setEmail(email.getText().toString());
                                    user.setName(username.getText().toString());
                                    user.setExperience(Integer.valueOf(experience.getText().toString()));
                                    user.setSpecialization(specialization.getText().toString());
                                    FirebaseUtils.saveVeterinarianToDatabase(authUser.getUid(), user);
                                   updateUI(VetPageActivity.class);
                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Register Failed...", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }

    public void updateUI(Class<?> activityTo) {
        Intent intent = new Intent(SignUpVeterinarianActivity.this, activityTo);
        startActivity(intent);
    }
}
