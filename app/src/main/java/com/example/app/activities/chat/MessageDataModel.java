package com.example.app.activities.chat;

import androidx.annotation.NonNull;

import java.util.Comparator;

public class MessageDataModel {
    private String id;
    private String sender;
    private String dateTime;
    private String text;
    private Boolean read;
    public static Comparator<MessageDataModel> compareByDate = new Comparator<MessageDataModel>() {
        @Override
        public int compare(MessageDataModel o1, MessageDataModel o2) {
            return o1.getDateTime().compareTo( o2.getDateTime() );
        }
    };
    public MessageDataModel() {
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getRead() {
        return read;
    }


    public static Comparator<MessageDataModel> getCompareByDate() {
        return compareByDate;
    }

    public static void setCompareByDate(Comparator<MessageDataModel> compareByDate) {
        MessageDataModel.compareByDate = compareByDate;
    }

    public MessageDataModel(@NonNull String sender, @NonNull String dateTime, @NonNull String text) {
        this.sender = sender;
        this.dateTime = dateTime;
        this.text = text;
        this.read = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime =dateTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
